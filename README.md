<h1>CDSD - Configuration Driven Socrata DataTables</h1>

Provides a completely declarative mechanism for defining an HTML table that uses attributes to configure a connection to
the Socrata Open Data API. This configuration does not interfere with DataTables built-in attribute driven 
confirguration for standard options such as using `data-order='[[2, "desc"], [1, "asc"]]'` on the table element to 
inform datatables of the default ordering. In fact, DADC, reads the sort order set by DataTables and will pass this sort 
to the server-side processing. See https://datatables.net/manual/options for using HTML 5 data attributes.

<h3>Configuration:</h3>

**Configuration on the `<table>` element**
* **data-od-socrata-domain** is the Socrata domain. For example, "https://data-od-socrata-domain/resource/...json"
* **data-od-dsid** is the Socrata dataset identifier. Also known as the 4x4
* **data-od-fixed-filter** provides a base filter so if your total rows should be a subset of the overall dataset, this attribute will essentially project a view that is a subselect.
* **data-od-final-sort** provides a final sort that will be applied after all other sorting. This is necessary to guarantee the order of rows for paging. Use the psudo-column :id to sort on the dataset's ID column without needing to specify the exact column name.

**Configuration on the `<tr>` element**
* **data-od-col** is a list of additional columns you want to select that are not associated with a DataTables column. This can be useful or internal row ids or custom renders that rely on the additional value without having to include it in the dable data/display.

**Configuration on the `<th>` elements**
* **data-od-col** is the corresponding column to select from the dataset. Any Socrata API function that can be called on a column can be used. Apply an alias so the name of the resulting column is known.
* data-od-col-alias is an alias that will be used for the column name similar to sql `select x as more_readable_name`.
* data-od-col-missing-value is a default value when the socrata dataset returns null. Socrata does not return a column reference for missing values. CDSD will by default inject an empty string and this attribute can be used to override that behavior with an alternate string.
* data-od-col-type is not used but is intended to identify the type for a future formatting enhancement or to explicity provide types to DataTables.

<h3>Example table configuration:</h3>

```html
<table id="reporting-history" class="dataTable" style="width:100%"
       data-od-socrata-domain="data.wa.gov"
       data-od-dsid="7qr9-q2c9"
       data-od-fixed-filter='where election_year > 2009'
       data-od-final-sort='report_number asc'>
   <thead>
   <tr data-od-col="origin, committee_id">
      <th data-od-col="report_number" data-od-col-type="text" data-od-col-missing-value="N/A">Report Number</th>
      <th data-od-col="filer_name" data-od-col-type="text" >Name</th>
      <th data-od-col="election_year" data-od-col-type="number" data-od-col-missing-value="0">Election Year</th>
      <th data-od-col="date_trunc_ymd(receipt_date)" data-od-col-alias="receipt_date" data-od-col-type="calendar_date" data-od-col-missing-value="" >Received</th>
      <th data-od-col="report_from" data-od-col-type="calendar_date">Period Start</th>
      <th data-od-col="report_through" data-od-col-type="calendar_date" data-od-col-missing-value="" >Period End</th>
      <th data-od-col="url.url" data-od-col-type="text" data-od-col-alias="url" data-od-col-missing-value="">Report</th>
   </tr>
   </thead>
</table>
```

<h3>Usage:</h3>

Just call configure you tables as in the example and DADC's `configureDadcTables()` function in your document.ready or equivalent function.

