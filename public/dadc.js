function configureDadcTables() {
  // Get all the tables with the class dataTable
  $('table.dataTable').each(function (index, table) {
    if (table instanceof jQuery && $.fn.dataTable.isDataTable(table)) {
      return;
    }
    table = $(table);
    const configData = table.data();
    const baseUrl = `https://${configData.dadcSocrataDomain}/resource/${configData.dadcDsid}.json`;
    const socrataAppToken = configData.dadcSocrataAppToken;

    const selectList = [];
    const columns = [];
    const headRow = $(table.find("tHead").find("tr"));
    const headCells = headRow.find("th");
    const rowProtoType = {};
    headCells.each(function (index, cell) {
      // Column type defaults to text and alias defaults to column
      const dadcCol = $(cell).data('dadcCol');
      const dadcColAlias = $(cell).data('dadcColAlias');
      const dadcColMissingValue = $(cell).data('dadcColMissingValue') ?? "";
      const dadcColGetDistinctValues = $(cell).data('dadcColGetDistinctValues') ?? false;
      const dadcColType = $(cell).data('dadcColType') ?? "text";
      const dadcRender = $(cell).data('dadcRender');
      const dadcSearchType = $(cell).data('dadcSearchType');

      var selectItem = dadcCol;
      if (dadcColAlias) {
        selectItem += ` as ${dadcColAlias}`;
      }
      selectList.push(selectItem);

      const column = {name: dadcColAlias ?? dadcCol, data: dadcColAlias ?? dadcCol, dadcCol: dadcCol};

      // Store values on the columns to be used by the fetcher ajax. Not using the built in DataTables types because of
      // the server-side handling and wanting to have particular handling based on Socrata types.
      if (dadcColType) column.dadcColType = dadcColType;
      if (dadcRender) column.dadcRender = dadcRender;
      if (dadcSearchType) column.dadcSearchType = dadcSearchType;
      if (dadcColGetDistinctValues) column.dadcColGetDistinctValues = dadcColGetDistinctValues;

      columns.push(column);

      // The row prototype contains all columns because Socrata only returns columns with values. It also specifies the
      // the default value when the column is not present. Datatables needs all the columns.
      rowProtoType[dadcColAlias ?? dadcCol] = dadcColMissingValue;


    });

    // Append additional columns to the select list. These are not part of the table but available to DataTables
    const extraColumns = headRow.data('dadcCol') ? headRow.data('dadcCol').split(',') : [];
    selectList.push(...extraColumns);

    // Build the filter list
    const baseFilter = configData.dadcFixedFilter ?? '';

    // The dataset :id psudo-column is used if none is provided. :id is whatever the is the id column, regardless of the
    // column name. Having this always reliable final sort is important so paging always happens in the same order. For
    // example, sorting by name will actually sort by name, id to force the same order even when a name is not unique.
    const finalSort = configData.dadcFinalSort ?? ':id';

    const dtSettings = {
      processing: true,
      serverSide: true,
      columns: columns,

      //================================================================================================================

      ajax: function (data, callback, settings) {

        // Loop through the data columns and get the search object data.columns[n]search.value. There is also a
        // data.columns[n]search.regex that could probably be used to determine if we should use = or contains.
        var filterList = [baseFilter];
        data.columns.forEach(function (item, index) {
          if (item.search.value) {
            const whereStatementItem = buildWhereStatementItem(item.search.value, columns[index]);
            filterList.push(whereStatementItem);
          }
        });
        filterList = filterList.filter(item => item.trim().length > 0);
        if (filterList.length > 0) {
          // buildWhereStatement preceeds each statement with "and" so we need to remove "and" and use "where" on the
          // first one.
          if (filterList[0].trim().indexOf('and') === 0) {
            filterList[0] = `where ${filterList[0].trim().substring(3)}`;
          }
        }

        // Finalize the select filter
        const selectFilter = filterList.join(' ');

        // Get the sort columns by using the data field of the settings for the columns being sorted on. It would be
        // nice to know when the finalSort column is already part of the sort criteria and not apply it but it gets
        // problematic when the :id psudo-column is used and Socrata is fine with something like "order by name asc,
        // name desc so it's not worth the complication.
        let sortColumns = [];
        data.order.forEach(function (item, index) {
          sortColumns.push(`${columns[item.column].data} ${item.dir}`);
        });
        sortColumns.push(finalSort);

        // Get the page size and offset
        const limit = data.length;
        const offset = data.start;

        const queryString = encodeURIComponent(`select ${selectList.join(',')} 
              ${selectFilter} order by ${sortColumns.join(',')} limit ${limit} offset ${offset}`);

        // -- FINAL URLs FOR FETCHING THE DATA -------------------------------------------------------------------------
        const allRowCountUrl = `${baseUrl}?$query=select count(*) ${encodeURIComponent(baseFilter)}`;
        const filteredRowCountUrl = `${baseUrl}?$query=select count(*) ${encodeURIComponent(selectFilter)}`;
        const pagedUrl = `${baseUrl}?$query=${queryString}`;

        // A template of the url to fetch the distinct values of a column which can be used to populate select lists.
        // This needs to apply baseFilter because if the base is all candidates, the filters should not include values
        // that are only for non-candidates
        const columnFilterTemplateUrl =
          `${baseUrl}?$query=select distinct %column% as %alias% ${encodeURIComponent(baseFilter)}`;
        // -------------------------------------------------------------------------------------------------------------

        // Storing stuff we need to persist across ajax calls might not be the best approach but we need a way so that
        // we don't have to reload the total row count or distinct column values every time someone pages. For example,
        // a sort operation won't change the filtered count. There is a possible timing issue where ajax requests run
        // out of order and a different request changes these values. Using the settings object is cautioned against in
        // the datatables manual unless you are writing a plugin but it seemed like the only option other than maybe
        // writing something to the dom but that seemed really messy, especially with the possibility of multiple tables
        // on a page.
        if (!settings.dadcSettings) {
          settings.dadcSettings = {};
        }

        (async function (draw, dadcSettings, socrataAppToken, selectFilter, pagedUrl, filteredRowCountUrl,
                         allRowCountUrl,columnFilterTemplateUrl) {

          // If the select filter changed, store the new one and null out the saved value. The select filter does not
          // change on a page event.
          if (!dadcSettings.selectFilter || dadcSettings.selectFilter !== selectFilter) {
            dadcSettings.selectFilter = selectFilter;
            dadcSettings.filteredRowCount = null;
          }

          let resultRows = fetch(pagedUrl, {headers: {'X-App-Token': socrataAppToken}}).then(response => response.json());
          let resultFilteredRowCount = dadcSettings.filteredRowCount ?
            dadcSettings.filteredRowCount :
            fetch(filteredRowCountUrl, {headers: {'X-App-Token': socrataAppToken}}).then(response => response.json());
          let resultAllRowCount = dadcSettings.allRowCount ?
            dadcSettings.allRowCount :
            fetch(allRowCountUrl, {headers: {'X-App-Token': socrataAppToken}}).then(response => response.json());

          // Get the values for columns that have dadcColGetDistinctValues set to true so that they can be added to the
          // data returned to datatables.
          let resultColumnFilterValues = dadcSettings.columnFilterValues ?
            dadcSettings.columnFilterValues :
            getColumnFilterValues(columns, columnFilterTemplateUrl, socrataAppToken);

          let result = await Promise.all([resultRows, resultFilteredRowCount, resultAllRowCount, resultColumnFilterValues]);

          //Store the values that can be persisted onto the settings object if they don't already exist.
          if (!dadcSettings.filteredRowCount) dadcSettings.filteredRowCount = result[1][0].count;
          if (!dadcSettings.allRowCount) dadcSettings.allRowCount = result[2][0].count;
          if (!dadcSettings.columnFilterValues) dadcSettings.columnFilterValues = result[3]


          resultRows = result[0];

          // The json result of rows only contains the properties with values but datatables requires all columns so we
          // apply the row prototype to make sure all the properties are there.
          resultRows.forEach(function (item, index, array) {
            array[index] = {...rowProtoType, ...item};
          });

          let json = {
            "draw": draw,
            "recordsTotal": dadcSettings.allRowCount,
            "recordsFiltered": dadcSettings.filteredRowCount,
            "yadcf_data_6": ["2007", "2020"],
            "data": resultRows
          }

          callback((json));

        })(data.draw, settings.dadcSettings, socrataAppToken, selectFilter, pagedUrl, filteredRowCountUrl,
          allRowCountUrl, columnFilterTemplateUrl);


      }
    };

    let oTable = $(table).DataTable(dtSettings);
    const yadcfSettings = getYadcfSettings(table);
    yadcf.init(oTable, yadcfSettings);
  });
}


/**
 * For all the columns that require a distinct value list, fetch the distinct values for that column.
 * @param columns
 * @param columnFilterUrl
 * @param socrataAppToken
 * @returns {Promise<unknown[]>} An array of the distinct values.
 */
async function getColumnFilterValues(columns, columnFilterUrl, socrataAppToken) {
  let columnFilterValues = [];
  columns.forEach(function (column, index) {
    const url = columnFilterUrl.replace('%column%', column.dadcCol).replace('%alias%', column.name);
    columnFilterValues[index] = column.dadcColGetDistinctValues ?
      fetch(url, {headers: {'X-App-Token': socrataAppToken}}).then(response => response.json()).then(result => result.map(item => item[column.name])) :
      null
    }
  );
  return Promise.all(columnFilterValues);
}

/**
 * Retrieves Yet Another DataTables configuration from the DOM data attributes and returns a settings array that can be
 * passed to yadcf.init().
 * @param table jQuery table
 * @returns [] yadcf settings array
 */
function getYadcfSettings(table) {
  const yadcfSettings = [];
  const headCells = table.find("tHead").find("tr").find("th");
  headCells.each(function (index, cell) {
    const attributesHavingPrefix = getDataAttributesHavingPrefix($(cell).data(), 'yadcf')
    if (Object.keys(attributesHavingPrefix).length > 0) {
      yadcfSettings.push({column_number: index, ...attributesHavingPrefix});
    }
  });
  return yadcfSettings;
}

function buildWhereStatementItem(searchValue, column) {

  // We really need a way to cancel the filter option and not have the datatable refresh or do anything if the combined
  // total action isn't going to lead to a change but there doesn't seem to be a way to cancel the operation.


  let statements = [];
  const searchtype = Array.isArray(column.dadcSearchType) ? column.dadcSearchType[0] : column.dadcSearchType;

  // Multi-select returns the terms pipe separated by default. When multi-select is used, the where statement item needs
  // to be wrapped in parens so for year: 2007|2008 it should produce (year=2007 or year=2008)
  const searchValues = searchValue.split("|");
  searchValues.forEach(searchValue => {
    let statement = "";
    switch (column.dadcColType) {
      case 'text':
        switch (searchtype) {
          case 'equals':
            // Case insensitive equality match
            statement = `lower(${column.dadcCol}) = "${searchValue.toLowerCase()}"`

            break;
          case 'text-tokens':
            // Split on whitespace, remove all extra empty terms and terms that are less than three characters and match
            // all terms in any order
          {
            const terms = searchValue.split(/[ ,]+/).filter(Boolean).filter(word => word.length >= 3);
            const tokenStatements = terms.map(term => `lower(${column.dadcCol}) like "%${term.toLowerCase()}%"`);
            if (tokenStatements.length > 0) {
              statement = `(${tokenStatements.join(" and ")})`;
            } // else return empty statement
          }
            break;
          case 'text-tokens-ordered':
            // Require all tokens in the same order. search "Jay Inslee" = "%jay%inslee%" matches "Jayleen R. Inslee Jr."
          {
            const terms = searchValue.split(/[ ,]+/).filter(Boolean).filter(word => word.length >= 3).map(term => term.toLowerCase());
            if (terms.length) {
              statement = `lower(${column.dadcCol}) like "%${terms.join("%")}%"`;
            } // else return empty statement
          }
            break;
          case 'like':
          default: //
            statement = `lower(${column.dadcCol}) like "%${searchValue.toLowerCase()}%"`
        }
        break;
      case 'number':
        switch (searchtype) {
          case 'number-range':
            // The yadcf control provides a single value of the form n-yadcf_delim-n where n is a number, the empty string
            // or NaN. It is important that the empty string is not treated like 0. Unfortunately yadcf passes the string
            // of whitespace as 0 so nothing we can do about it.
            const components = searchValue.split('-yadcf_delim-').map(item => {
              return (item === '' || !Number.isFinite(+item)) ? null : +item;
            })
            // The null comparison is important because 0 is a valid search value and null is not.
            if (components[0] != null && components[1] != null) {
              statement = `(${column.dadcCol} >= ${components[0]} and ${column.dadcCol} <= ${components[1]})`
            } else if (components[0] != null && components[1] == null) {
              statement = `${column.dadcCol} >= ${components[0]}`
            } else {
              statement = `${column.dadcCol} <= ${components[1]}`
            }
            break;
          default:
            statement = `${column.dadcCol} = ${searchValue}`
            break;
        }
        break;
      case 'dateTime':
        // On Socrata, dates are stored as dateTime. If we are just matching on date, we can either assume that all of the
        // data have time = 00:00:00.000 or use Socrata's date_trunc_ymd() function or always use range comparisons so
        // when looking for 01/21/1966, query date >= 1966-01-21 and date < 1966-01-22; Using date_trunc_ymd() is very
        // slow compared to query date >= 1966-01-21 and date < 1966-01-22 so that's what's being used.
        const dateFormat = Array.isArray(column.dadcSearchType) ? column.dadcSearchType[1] : null;
        switch (searchtype) {
          case 'date-range':
            // The yadcf control provides a single value of the form d-yadcf_delim-d where d is a number of the form
            // MM/dd/yyyy. If either date is not filled in, d is the empty string.
            const components = searchValue.split('-yadcf_delim-').map((component, index) =>
              component ? moment(component, dateFormat).add(index, 'days') : null);

            let allDatesAreValidOrNull = true;
            components.forEach(component => {
              if (component === null || component.isValid()) return allDatesAreValidOrNull;
              else return false;
            });

            if (allDatesAreValidOrNull) {
              if (components[0] && components[1]) {
                statement = `(${column.dadcCol} >= "${components[0].format('YYYY-MM-DD')}" and ${column.dadcCol} < "${components[1]}")`
              } else if (components[0]) {
                statement = `${column.dadcCol} >= "${components[0].format('YYYY-MM-DD')}"`
              } else {
                statement = `${column.dadcCol} < "${components[1].format('YYYY-MM-DD')}"`
              }
            } else {
              // In the case of an invalid date, the best action is probably to flag an error but it seems that the table
              // data always wants to reload. The other option is to just display no rows in the result. This is kind-of
              // a kludge but it works without changing the rest of the flow.
              statement = '1 = 2'; // Always false so no rows.
            }
            break;
          case 'date':
          default:
            let startDate = moment(searchValue, dateFormat, true);

            if (startDate.isValid()) {
              // The endDate is day +1 to account for values in the data that may have a time(< day+1)
              let endDate = startDate.clone().add(1, 'days');
              const endISODateString = null;
              statement = `(${column.dadcCol} >= "${startDate.format('YYYY-MM-DD')}" and ${column.dadcCol} < "${endDate.format('YYYY-MM-DD')}")`
            } else {
              // In the case of an invalid date, the best action is probably to flag an error but it seems that the table
              // data always wants to reload. The other option is to just display no rows in the result. This is kind-of
              // a kludge but it works without changing the rest of the flow.
              statement = '1 = 2'; // Always false so no rows.
            }
            break;
        }
        break;
      default:
        // Treat like text including quotes. Case insensitive equality match
        statement = `lower(${column.dadcCol}) = "${searchValue.toLowerCase()}"`
    }
    statements.push(statement);
  });

  // Because of the way the statements are built, it's possible that some of them might be the empty string so we can
  // just filter them out
  statements = statements.filter(item => item.trim().length > 0);

  let result = "";
  // If statements has one or more statement, wrap in parens and join with or
  if (statements.length > 1) {
    result = ` and (${statements.join(" or ")})`;
  }
  else if (statements.length === 1){
    result = ` and ${statements[0]}`;
  }

  return result;
}

/**
 * Gets all the attributes having prefix. Removes the prefix and makes the first character lowercase. This is because
 * getting the data attributes returns them as camel case. For example the data attribute data-yadcf-filter_type in html
 * woll become yadcfFilter_type as a data attribute. When we just want "filter_type".
 * @param dataObject
 * @param prefix
 * @returns {{}} An object of key: value properties with all data attributes matching prefix
 */
function getDataAttributesHavingPrefix(dataObject, prefix) {
  const attributes = {};
  for (const prop in dataObject) {
    if (dataObject.hasOwnProperty(prop) && prop.indexOf(prefix) === 0) {
      let propName = prop.replace(prefix, '')
      propName = propName && propName[0].toLowerCase() + propName.slice(1);
      let value;
      // The value of attribute.value might be a string or a json object. Using an exception for flow control is stupid
      // but there's now way to determine if the string is a serialized object except to just try it. There's no
      // tryParse function in javascript.
      try {
        value = JSON.parse(dataObject[prop]);
      } catch (e) {
        value = dataObject[prop];
      }
      attributes[propName] = value;
    }
  }
  return attributes;
}